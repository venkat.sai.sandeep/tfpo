json.extract! tag, :id, :name, :description, :taggable_id, :taggable_type, :created_at, :updated_at
json.url tag_url(tag, format: :json)
