class AddAttachmentPhotoToInfos < ActiveRecord::Migration
  def self.up
      add_attachment :infos, :photo
  end

  def self.down
    remove_attachment :infos, :photo
  end
end
