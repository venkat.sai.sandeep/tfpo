class AddAttachmentPhotoToNews < ActiveRecord::Migration
  def self.up
      add_attachment :news,  :photo
  end

  def self.down
    remove_attachment :news, :photo
  end
end
