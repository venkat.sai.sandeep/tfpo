class CreateInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :infos do |t|
      t.text :body
      t.string :subject

      t.timestamps
    end
  end
end
