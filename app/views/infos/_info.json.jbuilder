json.extract! info, :id, :body, :subject, :created_at, :updated_at
json.url info_url(info, format: :json)
