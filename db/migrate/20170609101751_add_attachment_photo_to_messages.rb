class AddAttachmentPhotoToMessages < ActiveRecord::Migration
  def self.up
      add_attachment :messages, :photo
  end

  def self.down
    remove_attachment :messages, :photo
  end
end
