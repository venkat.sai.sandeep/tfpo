class ApplicationController < ActionController::Base
  # to authenticate which user logged in
  before_action :authenticate_user!

  protect_from_forgery with: :exception

  def welcome
    render 'home/home.html.erb'
  end

end
